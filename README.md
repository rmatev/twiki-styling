TWiki styling
=============

Styles for making a TWiki cleaner, prettier, and more readable.

![Example pages styled with the CSS in this repository.](preview.png)

This repository contains a CSS file which can be uploaded to your TWiki user 
page, which you can then tell TWiki to use.

There is also an optional JavaScript file, which will replace the default
LaTeX-rendered-as-images with LaTeX rendered by [KaTeX][katex].

Style Installation instructions
-------------------------------

TWiki allows each user to define preferences on their personal user page. My 
user page, as an example, is at:

```
https://twiki.cern.ch/twiki/bin/view/Main/AlexPearce
```

There is one set of preferences that allows the user to define a URL to a CSS 
file, which is then used to style the site.

To ‘install’ the CSS style in this repository, download the file to your 
machine, and then upload it to your TWiki user page.
Find the URL TWiki gives it, then edit your user page in the ‘raw edit’ mode 
and add this fragment:

```
   * Set USERSTYLEURL = %PUBURLPATH%/Main/AlexPearce/twiki.css
```

The preceding spaces are important. TWiki requires the preferences to be 
defined in a unordered list, and in TWiki markup these are denoted with three 
spaces followed by an asterisk `*`.

`%PUBURLPATH%` is a variable that is expanded by TWiki. For me, it expands to 
the value ` /twiki/pub`, which happens to be the prefix of the CSS file’s 
upload path.

### Fonts

The stylesheet automaticallys loads the ‘Source’ family of fonts from Adobe.
These are good-looking, freely available, open source fonts available to 
download on Github:

* [Source Serif Pro][source-serif]
* [Source Sans Pro][source-sans]
* [Source Code Pro][source-code]

The stylesheet takes versions hosted on [Google Fonts][gfonts].

JavaScript Installation instructions
------------------------------------

There are many ways of injecting custom JavaScript into a web page. I use 
[Tampermonkey][tampermonkey].

To install the TWiki LaTeX prettification script, install something like 
Tampermonkey, click on the Tampermonkey button that appears in your browser’s 
toolbar, then click ‘Create a new script…’. Paste in the contents of the 
JavaScript file from this repository, and click the ‘Save’ in the Tampermonkey 
editor interface.

When you next visit a page whose URL matches the script’s matching rule 
(currently `https://twiki.cern.ch/*`), the script will be loaded. You can check 
that it’s working by clicking the Tampermonkey toolbar button, and seeing that 
that script is listed as active.

Contributions
-------------

The styling is by no means complete, and contributions are most welcome, just 
open a merge request.
In particular, tables have the default TWiki styling. I don’t find it 
outrageously offensive, but it could be better.

[katex]: https://khan.github.io/KaTeX/
[tampermonkey]: http://tampermonkey.net
[source-serif]: https://github.com/adobe-fonts/source-serif-pro
[source-sans]: https://github.com/adobe-fonts/source-sans-pro
[source-code]: https://github.com/adobe-fonts/source-code-pro
[gfonts]: https://fonts.google.com
