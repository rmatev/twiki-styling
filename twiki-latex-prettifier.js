// ==UserScript==
// @name         TWiki LaTeX prettifier
// @namespace    https://alexpearce.me/
// @version      0.1
// @description  Replace low-resolution rendered LaTeX with KaTEX
// @author       Alex Pearce
// @match        https://twiki.cern.ch/*
// @grant        none
// ==/UserScript==

(function(window, document, undefined) {
    'use strict';

    var renderLaTeX = function() {
        var head = document.getElementsByTagName('head')[0];

        var link = document.createElement('link');
        link.rel = 'stylesheet';
        link.type = 'text/css';
        link.href = '//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.7.1/katex.min.css';

        var script = document.createElement('script');
        script.type= 'text/javascript';
        script.src = '//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.7.1/katex.min.js';
        script.onload = function() {
            var imgs = document.getElementsByTagName('img'),
                imgsToReplace = [], spansToRender = [],
                i, img, alt, latex, span;
            for (i = 0; i < imgs.length; i++) {
                img = imgs[i];
                alt = img.alt;
                if (!(alt.startsWith('$') && alt.endsWith('$'))) {
                    continue;
                }
                latex = alt.substr(1, alt.length - 2);

                span = document.createElement('span');
                span.dataset.alt = alt;
                katex.render(latex, span);
                imgsToReplace.push(img);
                spansToRender.push(span);
            }

            for (i = 0; i < spansToRender.length; i++) {
                img = imgsToReplace[i];
                span = spansToRender[i];
                img.parentNode.replaceChild(span, img);
            }
        };

        head.appendChild(link);
        head.appendChild(script);
    };

    renderLaTeX();
})(window, document);
